// repo at: https://gitlab.com/dvirr/mamas.dev

const startDate = new Date('May 09, 2023 08:30:06').getTime();

const coundownElement = document.getElementById('countdown');

(function loop() {
  const distance = startDate - new Date().getTime();
  const days = Math.floor(distance / (1000 * 60 * 60 * 24));
  const hours = Math.floor(
    (distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60)
  );
  const minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
  const seconds = Math.floor((distance % (1000 * 60)) / 1000);

  if (distance < 0) {
    coundownElement.innerText = 'GO!';
  } else {
    coundownElement.innerText = `${days}d ${hours}h ${minutes}m ${seconds}s `;
    setTimeout(loop, 500);
  }
})();
